package v1.inventory

import javax.inject.{Inject, Singleton}
import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext
import play.api.{Logger, MarkerContext}

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.util.Random

final case class InventoryData(id: InventoryId, sku: String, var qty: Int, containerId: String)

class InventoryId private(val underlying: Int) extends AnyVal {
  override def toString: String = underlying.toString
}

object InventoryId {
  def apply(raw: String): InventoryId = {
    require(raw != null)
    new InventoryId(Integer.parseInt(raw))
  }
}


class InventoryExecutionContext @Inject()(actorSystem: ActorSystem) extends CustomExecutionContext(actorSystem, "repository.dispatcher")

/**
  * A pure non-blocking interface for the InventoryRepository.
  */
trait InventoryRepository {
  def create(data: InventoryData)(implicit mc: MarkerContext): Future[InventoryId]

  def transfer(
                sku: String,
                qty: Int,
                fromContainerId: String,
                toContainerId: String,
              )(implicit mc: MarkerContext): Future[InventoryData]

  def list()(implicit mc: MarkerContext): Future[Iterable[InventoryData]]

  def get(id: InventoryId)(implicit mc: MarkerContext): Future[Option[InventoryData]]
}

/**
  * A trivial implementation for the Inventory Repository.
  *
  * A custom execution context is used here to establish that blocking operations should be
  * executed in a different thread than Play's ExecutionContext, which is used for CPU bound tasks
  * such as rendering.
  */
@Singleton
class InventoryRepositoryImpl @Inject()()(implicit ec: InventoryExecutionContext) extends InventoryRepository {

  private val logger = Logger(this.getClass)

  private val inventoryList = ListBuffer(
    InventoryData(InventoryId("1"), "SKU-01", 1, "CONTAINER-X01"),
    InventoryData(InventoryId("2"), "SKU-02", 2, "CONTAINER-X02"),
    InventoryData(InventoryId("3"), "SKU-03", 0, "CONTAINER-X03"),
    InventoryData(InventoryId("4"), "SKU-04", 5, "CONTAINER-X04"),
    InventoryData(InventoryId("5"), "SKU-05", 1000, "CONTAINER-X05")
  )

  override def list()(implicit mc: MarkerContext): Future[Iterable[InventoryData]] = {
    Future {
      logger.trace(s"list: ")
      inventoryList
    }
  }

  override def get(id: InventoryId)(implicit mc: MarkerContext): Future[Option[InventoryData]] = {
    Future {
      logger.trace(s"get: id = $id")
      inventoryList.find(inventory => inventory.id == id)
    }
  }

  def create(data: InventoryData)(implicit mc: MarkerContext): Future[InventoryId] = {
    Future {
      logger.trace(s"create: data = $data")
      inventoryList.append(data)
      data.id
    }
  }

  def transfer(
                sku: String,
                qty: Int,
                fromContainerId: String,
                toContainerId: String,
              )(implicit mc: MarkerContext): Future[InventoryData] = {
    Future {
      logger.trace(s"transfer: $sku $qty $fromContainerId $toContainerId")

      val fromData = inventoryList.find(p => p.containerId == fromContainerId && p.sku == sku)
      var toData = inventoryList.find(p => p.containerId == toContainerId && p.sku == sku)

      // TODO - no validation is done at all
      if (fromData.isDefined) fromData.get.qty = fromData.get.qty - qty
      else throw new RuntimeException("Must have fromContainer")

      if (toData.isDefined && toData.get.sku == sku) {
        toData.get.qty = toData.get.qty + qty
      } else {
        // This wont work because it wants the string to convert to an Int at some stage, I really don't get
        // why they made this special object for the id.
        //val randomId = randomUUID().toString
        val randomInt = Random.nextInt().toString()

        toData = Option(InventoryData(InventoryId(randomInt), sku, qty, toContainerId))
        inventoryList.append(toData.get)
      }

      toData.get
    }
  }

}
