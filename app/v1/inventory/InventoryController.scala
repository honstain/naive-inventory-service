package v1.inventory

import javax.inject.Inject

import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

case class InventoryFormInput(sku: String, qty: Int, containerId: String)

case class InventoryTransferFormInput(sku: String, qty: Int, fromContainerId: String, toContainerId: String)

/**
  * Takes HTTP requests and produces JSON.
  */
class InventoryController @Inject()(cc: InventoryControllerComponents)(implicit ec: ExecutionContext)
    extends InventoryBaseController(cc) {

  private val logger = Logger(getClass)

  private val form: Form[InventoryFormInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "sku" -> nonEmptyText,
        "qty" -> number,
        "containerId" -> nonEmptyText,
      )(InventoryFormInput.apply)(InventoryFormInput.unapply)
    )
  }

  private val transferForm: Form[InventoryTransferFormInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "sku" -> nonEmptyText,
        "qty" -> number,
        "fromContainerId" -> nonEmptyText,
        "toContainerId" -> nonEmptyText,
      )(InventoryTransferFormInput.apply)(InventoryTransferFormInput.unapply)
    )
  }

  def index: Action[AnyContent] = InventoryAction.async { implicit request =>
    logger.trace("index: ")
    inventoryResourceHandler.find.map { posts =>
      Ok(Json.toJson(posts))
    }
  }

  def process: Action[AnyContent] = InventoryAction.async { implicit request =>
    logger.trace("process: ")
    processJsonPost()
  }

  def show(id: String): Action[AnyContent] = InventoryAction.async { implicit request =>
    logger.trace(s"show: id = $id")
    inventoryResourceHandler.lookup(id).map { post =>
      Ok(Json.toJson(post))
    }
  }

  def transfer: Action[AnyContent] = InventoryAction.async { implicit  request =>
    logger.trace("transfer: ")
    processTransfer()
  }

  private def processTransfer[A]()(implicit  request: InventoryRequest[A]) : Future[Result] = {
    def failure(badForm: Form[InventoryTransferFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: InventoryTransferFormInput) = {
      inventoryResourceHandler.transfer(input).map { post =>
        Created(Json.toJson(post)).withHeaders(LOCATION -> post.link)
      }
    }

    transferForm.bindFromRequest().fold(failure, success)
  }

  private def processJsonPost[A]()(implicit request: InventoryRequest[A]): Future[Result] = {
    def failure(badForm: Form[InventoryFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: InventoryFormInput) = {
      inventoryResourceHandler.create(input).map { post =>
        Created(Json.toJson(post)).withHeaders(LOCATION -> post.link)
      }
    }

    form.bindFromRequest().fold(failure, success)
  }
}
