package v1.inventory

import javax.inject.{Inject, Provider}

import play.api.MarkerContext

import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._

/**
  * DTO for displaying inventory information.
  */
case class InventoryResource(id: String, link: String, sku: String, qty: Int, containerId: String)

object InventoryResource {

  /**
    * Mapping to write a InventoryResource out as a JSON value.
    */
  implicit val implicitWrites = new Writes[InventoryResource] {
    def writes(inventory: InventoryResource): JsValue = {
      Json.obj(
        "id" -> inventory.id,
        "link" -> inventory.link,
        "sku" -> inventory.sku,
        "qty" -> inventory.qty,
        "containerId" -> inventory.containerId,
      )
    }
  }
}

/**
  * Controls access to the backend data, returning [[InventoryResource]]
  */
class InventoryResourceHandler @Inject()(
                                          routerProvider: Provider[InventoryRouter],
                                          inventoryRepository: InventoryRepository)(implicit ec: ExecutionContext) {

  def create(inventoryInput: InventoryFormInput)(implicit mc: MarkerContext): Future[InventoryResource] = {
    val data = InventoryData(InventoryId("999"), inventoryInput.sku, inventoryInput.qty, inventoryInput.containerId)

    inventoryRepository.create(data).map { id =>
      createInventoryResource(data)
    }
  }

  def transfer(inventoryTransferInput: InventoryTransferFormInput)(implicit mc: MarkerContext): Future[InventoryResource] = {
    inventoryRepository.transfer(
      inventoryTransferInput.sku,
      inventoryTransferInput.qty,
      inventoryTransferInput.fromContainerId,
      inventoryTransferInput.toContainerId
    ).map { data => createInventoryResource(data)}
  }

  def lookup(id: String)(implicit mc: MarkerContext): Future[Option[InventoryResource]] = {
    val inventoryFuture = inventoryRepository.get(InventoryId(id))
    inventoryFuture.map { maybePostData =>
      maybePostData.map { inventoryData =>
        createInventoryResource(inventoryData)
      }
    }
  }

  def find(implicit mc: MarkerContext): Future[Iterable[InventoryResource]] = {
    inventoryRepository.list().map { inventoryDataList =>
      inventoryDataList.map(inventoryData => createInventoryResource(inventoryData))
    }
  }

  private def createInventoryResource(p: InventoryData): InventoryResource = {
    InventoryResource(p.id.toString, routerProvider.get.link(p.id), p.sku, p.qty, p.containerId)
  }

}
